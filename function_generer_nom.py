def generer_nom(theme,first_name,last_name,univers):
    first_name_letter = first_name[univers[theme]['num_first_name']].upper()
    last_name_letter = last_name[univers[theme]['num_last_name']].upper()
    new_first_name = univers[theme]['First_name'][first_name_letter]
    new_last_name =univers[theme]['Last_name'][last_name_letter]
    complete_name="%s %s"%(new_first_name,new_last_name)
    return complete_name