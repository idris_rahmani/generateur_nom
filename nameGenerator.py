from tkinter import *
from tkinter.ttk import *
from function_generer_nom import generer_nom

univers = {
        'Supervillain' : {
            'num_last_name' : 1,
            'num_first_name' : 0,
            'First_name' : {
                'A' : 'The Evil',  
                'B' : 'The Mad',   
                'C' : 'The Big',   
                'D' : 'The Dangerous',     
                'E' : 'Captain',      
                'F' : 'The Ghostly',  
                'G' : 'Professor',      
                'H' : 'Doctor',     
                'I' : 'Phantom',    
                'J' : 'The Brutal',     
                'K' : 'The Unstoppable', 
                'L' : 'The Deadly',      
                'M' : 'The Dark',     
                'N' : 'The Crazy',
                'O' : 'The Diabolic',
                'P' : 'The Poison',
                'Q' : 'The Sinister',
                'R' : 'The Bloody',
                'S' : 'The Immortal',
                'T' : 'The Galactic',
                'U' : 'The Rancid',
                'V' : 'The Invisible',
                'W' : 'The Black',
                'X' : 'The Imperial',
                'Y' : 'The Mega',
                'Z' : 'The Grand',
            },
            'Last_name' : {
                'A' : 'Shadow',  
                'B' : 'Knight',   
                'C' : 'Tarantula',   
                'D' : 'Skull',     
                'E' : 'Mastermind',      
                'F' : 'Wizard',  
                'G' : 'Ninja',      
                'H' : 'Devil',     
                'I' : 'Freak',    
                'J' : 'Beast',     
                'K' : 'Criminal', 
                'L' : 'Master',      
                'M' : 'Menace',     
                'N' : 'Child',
                'O' : 'Corpse',
                'P' : 'Slayer',
                'Q' : 'Spider',
                'R' : 'Creature',
                'S' : 'Abomination',
                'T' : 'Monster',
                'U' : 'Vampire',
                'V' : 'Mutant',
                'W' : 'Robot',
                'X' : 'Conqueror',
                'Y' : 'Machine',
                'Z' : 'Clown',
            },
        },
        'Video Game' : {
            'num_last_name' : 2,
            'num_first_name' : 1,
            'First_name' : {
                'A' : 'The Legend Of',  
                'B' : 'Super',   
                'C' : 'Mecha',   
                'D' : 'Mark Of',     
                'E' : 'Hero Of',      
                'F' : 'Tomb',  
                'G' : 'God Of',      
                'H' : 'Metal',     
                'I' : 'Combat',    
                'J' : 'Flight',     
                'K' : 'Attack on', 
                'L' : 'Oregon',      
                'M' : 'Call Of',     
                'N' : 'World OF',
                'O' : 'Castle',
                'P' : 'Shadow Of The',
                'Q' : 'King O\'s',
                'R' : 'Rock',
                'S' : 'Little Big',
                'T' : 'Mortal',
                'U' : 'Wing',
                'V' : 'Silent',
                'W' : 'Golden',
                'X' : 'Street',
                'Y' : 'Knockout',
                'Z' : 'Space',
            },
            'Last_name' : {
                'A' : 'The Game',  
                'B' : 'Bros',   
                'C' : 'Souls',   
                'D' : 'Time',     
                'E' : 'Legend',      
                'F' : 'Warriors',  
                'G' : 'Dragons',      
                'H' : 'Revolution',     
                'I' : 'Speed',    
                'J' : 'Evil',     
                'K' : 'Earth Heroes', 
                'L' : 'Birds',      
                'M' : 'Strike',     
                'N' : 'Prime',
                'O' : 'Sports',
                'P' : 'Quest',
                'Q' : 'Warfare',
                'R' : 'Hunter',
                'S' : 'Adventures',
                'T' : 'Gods',
                'U' : '3000',
                'V' : 'Commander',
                'W' : 'Online',
                'X' : 'Tactics',
                'Y' : 'Ninja',
                'Z' : 'Defense',
            },
        },
        'Iron Man Suit' : {
            'num_last_name' : 0,
            'num_first_name' : 2,
            'First_name' : {
                'A' : 'Bunker',  
                'B' : 'Iron',   
                'C' : 'Mecha',   
                'D' : 'Metal',     
                'E' : 'Hulk',      
                'F' : 'Superior',  
                'G' : 'Reinforced',      
                'H' : 'Combat',     
                'I' : 'Experimental',    
                'J' : 'Resistance',     
                'K' : 'Forerunner', 
                'L' : 'Battle',      
                'M' : 'Warfare',     
                'N' : 'Ultimate',
                'O' : 'War',
                'P' : 'Frontline',
                'Q' : 'Advanced',
                'R' : 'Space',
                'S' : 'Guerilla',
                'T' : 'Supreme',
                'U' : 'Wing',
                'V' : 'Adamantium',
                'W' : 'Modular',
                'X' : 'Thor',
                'Y' : 'Armored',
                'Z' : 'Hammer',
            },
            'Last_name' : {
                'A' : 'Champion',  
                'B' : 'Machine',   
                'C' : 'Warrior',   
                'D' : 'Soldier',     
                'E' : 'Buster',      
                'F' : 'Armor',  
                'G' : 'Vanguard',      
                'H' : 'Exoskeleton',     
                'I' : 'Crusher',    
                'J' : 'Suit',     
                'K' : 'Enforcer', 
                'L' : 'Core',      
                'M' : 'Striker',     
                'N' : 'Breaker',
                'O' : 'Knight',
                'P' : 'Wrecker',
                'Q' : 'Shell',
                'R' : 'Patriot',
                'S' : 'Centurion',
                'T' : 'Star',
                'U' : 'Hero',
                'V' : 'Commander',
                'W' : 'Guard',
                'X' : 'Man',
                'Y' : 'System',
                'Z' : 'Drone',
            },
        },
    }

def show_result():
    complete_name=generer_nom(theme.get(),first_name.get(),last_name.get(),univers)
    label_question.configure(text="What is your %s Name?" % theme.get())
    label_surname.configure(text = complete_name)

window = Tk()
window.title("Name Generator")
window.resizable(0,0)

window.columnconfigure(1,weight=1)
window.columnconfigure(2,weight=1)
window.columnconfigure(3,weight=1)
window.columnconfigure(4,weight=1)

label_title = Label(window, text = "Name Generator", justify = "center",font=("Calibri",30))
label_title.grid(column=0, row=0, columnspan=4)

label_first_name = Label(window, text = "Prenom :",justify = "left",font=("Calibri",10))
label_first_name.grid(column=0,row=1)

first_name = StringVar() 
first_name.set("")
entry_first_name = Entry(window, textvariable = first_name)
entry_first_name.grid(column=1,row=1)

label_last_name = Label(window, text = "Nom :",justify = "left",font=("Calibri",10))
label_last_name.grid(column=2,row=1)

last_name = StringVar() 
last_name.set("")
entry_last_name = Entry(window, textvariable = last_name)
entry_last_name.grid(column=3,row=1)

label_theme = Label(window, text = "Choisissez le theme qui vous interesse :",justify = "left",font=("Calibri",10))
label_theme.grid(column=0,row=2,columnspan=2)

theme = StringVar()
combonbox_theme = Combobox(window, textvariable = theme, state='readonly', values=list(univers.keys()),font=("Calibri",10))
combonbox_theme.grid(column=2,row=2,columnspan=2)
combonbox_theme.current(0)

label_question = Label(window, text="What is your ____ Name?",justify = "center", font=("Calibri",15))
label_question.grid(column=0,row=4,columnspan=4)

label_surname = Label(window, text="", justify = "center", font=("Calibri",15))
label_surname.grid(column=0,row=5,columnspan=4)

button_submit = Button(window, text = "Soumettre", command = show_result)
button_submit.grid(column=1,row=3,columnspan=2)


window.mainloop()